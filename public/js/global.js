/* global Lynx*/
var objData = new Object();
for (let [key, value] of Object.entries(Lynx.settings)){
  Object.defineProperty(objData, key, {
    writable: true,
    value: value
  });
}

// eslint-disable-next-line no-unused-vars
const homeDir = objData.homeDir;