/* eslint-disable no-unused-vars */
/*global Metro, $*/
////////////
// Search //
////////////

// Abre Search Launcher
function openSearchLauncher(){
  Metro.dialog.open('#search')
  document.getElementById('searchBar').focus();
  closeSettingsPanel();
  closeMenu();
}

// Cierra Search Launcher
function closeSearchLauncher(){
  Metro.dialog.close('#search');
}

// Cambia Search Launcher
function toggleSearchLauncher(){
  Metro.dialog.toggle('#search');
  document.getElementById('searchBar').focus();
  closeSettingsPanel();
  closeMenu();
}

//////////////
// Settings //
//////////////

// Abre Settings Panel
function openSettingsPanel(){
  Metro.sidebar.open('#LynxSys');
  closeSearchLauncher();
  closeMenu();
}

// Cierra Settings Panel
function closeSettingsPanel(){
  Metro.charms.close("#settingsMenu");
  Metro.charms.close("#lynxSysAppearance");
  Metro.charms.close("#lynxSysBackground");
  Metro.charms.close("#lynxSysBehavior");
  Metro.charms.close("#lynxSysDevMenu");
  Metro.charms.close("#lynxSysUser");
  Metro.sidebar.close('#LynxSys');
}

// Toggle Settings Panel
function toggleSettingsPanel(){
  Metro.charms.close("#settingsMenu");
  Metro.charms.close("#lynxSysAppearance");
  Metro.charms.close("#lynxSysBehavior");
  Metro.charms.close("#lynxSysDevMenu");
  Metro.charms.close("#lynxSysUser");
  Metro.charms.close("#lynxSysBackground");
  Metro.sidebar.toggle('#LynxSys');
  closeSearchLauncher();
  closeMenu();
}

// Toggle LynxSysMenu
function toggleLynxSysTools(){
  Metro.charms.toggle("#settingsMenu");
  Metro.charms.close("#lynxSysAppearance");
  Metro.charms.close("#lynxSysBehavior");
  Metro.charms.close("#lynxSysDevMenu");
  Metro.charms.close("#lynxSysUser");
  Metro.charms.close("#lynxSysBackground");
}

// Toggle LynxSysAppearance
function toggleLynxSysAppearance(){
  Metro.charms.close("#settingsMenu");
  Metro.charms.toggle("#lynxSysAppearance");
  Metro.charms.close("#lynxSysBehavior");
  Metro.charms.close("#lynxSysDevMenu");
  Metro.charms.close("#lynxSysUser");
  Metro.charms.close("#lynxSysBackground");
}

// Toggle LynxSysBehavior
function toggleLynxSysBehavior(){
  Metro.charms.close("#settingsMenu");
  Metro.charms.close("#lynxSysAppearance");
  Metro.charms.toggle("#lynxSysBehavior");
  Metro.charms.close("#lynxSysDevMenu");
  Metro.charms.close("#lynxSysUser");
  Metro.charms.close("#lynxSysBackground");
}

// Toggle LynxSysDevMenu
function toggleLynxSysDevMenu(){
  Metro.charms.close("#settingsMenu");
  Metro.charms.close("#lynxSysAppearance");
  Metro.charms.close("#lynxSysBehavior");
  Metro.charms.toggle("#lynxSysDevMenu");
  Metro.charms.close("#lynxSysUser");
  Metro.charms.close("#lynxSysBackground");
}

// Toggle LynxSysUser
function toggleLynxSysUser(){
  Metro.charms.close("#settingsMenu");
  Metro.charms.close("#lynxSysAppearance");
  Metro.charms.close("#lynxSysBehavior");
  Metro.charms.close("#lynxSysDevMenu");
  Metro.charms.toggle("#lynxSysUser");
  Metro.charms.close("#lynxSysBackground");
}

// Toggle LynxSysBackground
function toggleLynxSysBackground(){
  Metro.charms.close("#settingsMenu");
  Metro.charms.close("#lynxSysAppearance");
  Metro.charms.close("#lynxSysBehavior");
  Metro.charms.close("#lynxSysDevMenu");
  Metro.charms.close("#lynxSysUser");
  Metro.charms.toggle("#lynxSysBackground");
}

//////////
// Menu //
//////////

// Abre Menu
function openMenu(){
  $('#Menu').data('charms').open();
  closeSearchLauncher();
  closeSettingsPanel();
}

// Cierra Menu
function closeMenu(){
  $('#Menu').data('charms').close();
}

// Toggle Menu
function toggleMenu(){
  $('#Menu').data('charms').toggle();
  closeSearchLauncher();
  closeSettingsPanel();
}
