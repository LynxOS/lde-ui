/* eslint-disable no-unused-vars */
/* global toggleSettingsPanel, LDK, toggleMenu, closeSettingsPanel
  openSettingsPanel, Lynx */

var desktop;
const LynxDesktopMain = class API {

    constructor() {
        this.visible = true
        this.searchActive = false
    }

    // Retorna el Elemento
    elem(element) {
        return document.querySelector(element)
    }

    // Retorna los elementos pedidos
    elems(elements) {
        return document.querySelectorAll(elements)
    }

    // Intercambia Panel de Herramientas
    toggleSettings() {
      toggleSettingsPanel();
    }

    // Intercambia Launcher
    toggleLauncher() {
      toggleMenu();
      this.toggleDesktop();
    }

    toggleDesktop() {
      LDK.Bridge.toggleDesktop()
    }

    hideInspector() {
      LDK.Bridge.hideInspector();
    }

    settingsSidenav() {
      console.log("fix");
    }

    appView() {
      return this.elem('.applications-wrapper')
    }

    closeSettings() {
      closeSettingsPanel();
    }

    openSettings() {
      openSettingsPanel();
    }

    closeApplications() {
      //this.openWidgets()
      let self = this
      LDK.Bridge.setPanelVisible(false)
    }

    get_categories() {
      let items = []
      for (let category in Lynx.menu) {
        items.push(category)
      }
      return items
    }

    get_search_items() {
      LDK.Bridge.updateMenu()
      let items = []
      let categories = this.get_categories()
      for (let category of categories) {
        let apps = Lynx.menu[`${category}`].apps
        for (let app of apps) {
          items.push(app)
        }
      }
      return items
    }

    matchQuery(item, searchQuery) {
      return item.name.toLowerCase().includes(searchQuery.toLowerCase()) || item.description.toLowerCase().includes(searchQuery.toLowerCase()) || item.keywords.toLowerCase().includes(searchQuery.toLowerCase())
    }

    closeWidgets() {
      desktop.elem("#floating-items").style.opacity = 0;
    }

    openWidgets() {
      desktop.elem("#floating-items").style.opacity = 1;
    }

    empty(el) {
      el.innerHTML = "";
    }
}


document.addEventListener('DOMContentLoaded', () => {
  desktop = new LynxDesktopMain();

  setInterval(showTime, 1000);

  function showTime() {
    let time = new Date()
    let hour = time.getHours()
    let min = time.getMinutes()
    hour = hour < 10 ? "0" + hour : hour
    min = min < 10 ? "0" + min : min
    let currentTime = hour + ":" + min
    desktop.elem("#time").innerHTML = currentTime
  }

  showTime()

  function debounce(callback, wait) {
    let timeout;
    return(...args) => {
      const context = this;
      clearTimeout(timeout);
      timeout = setTimeout(() => callback.apply(context, args), wait);
    };
  }

  for (let [key, value] of Object.entries(Lynx.settings)) {
    let button = desktop.elem(`#${key}-btn`)
    if (button != null) {
      button.checked = value
      button.addEventListener('click', function () {
        LDK.Bridge.saveSettings(key, button.checked)
      })
    }
  }

  let autoTileBtn = desktop.elem("#auto-tile-btn")
  autoTileBtn.checked = Lynx.settings.autoTile
  autoTileBtn.addEventListener('click', function () {
    LDK.Bridge.saveSettings("autoTile", autoTileBtn.checked)
  })

  let appView = desktop.elem(".applications-wrapper")
  appView.onmouseleave = function () {
    desktop.closeApplications()
  }
})
