import Vue from 'vue';

var windows = Vue.observable({
  windows: []
})

export default {
  install(Vue){
    Vue.prototype.$WM = windows;
  }
}