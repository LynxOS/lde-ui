/* global homeDir */
export default {
  install(Vue){
    Vue.prototype.$HOME = homeDir;
  }
}