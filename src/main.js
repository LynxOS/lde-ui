import Vue from 'vue';
import App from './App.vue';
import LynxHomePlugin from "./plugins/lynxHomePlugin.js"
import LynxWindowsPlugin from "./plugins/lynxWindowsPlugin.js"

Vue.config.productionTip = false;
Vue.use(LynxHomePlugin);
Vue.use(LynxWindowsPlugin);

window.App = new Vue({
  render: h => h(App),
}).$mount('#app');
